import 'dart:math';

import 'package:flutter/material.dart';

class StackedAppBarPage extends StatelessWidget {
  const StackedAppBarPage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            // First SliverAppBar
        
            SliverAppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.indigo,
              expandedHeight: 200,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Text(
                  'Book',
                ),
                background: Image.asset(
                  "assets/images/book.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (_, int index) {
                  return ListTile(
                    leading: Container(
                      padding: EdgeInsets.all(10),
                      color: Colors.indigo[100 * (index % 9)],
                      child: Text('Book $index'),
                    ),
                    title: Text('Image ${index + 1}', textScaleFactor: 2),
                  );
                },
                childCount: 20,
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.only(bottom: 16.0),
            ),
            SliverAppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.blue,
              expandedHeight: 200,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Text(
                  'Music',
                ),
                background: Image.asset(
                  "assets/images/music.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.only(bottom: 16.0),
            ),
            SliverGrid(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  return Container(
                    alignment: Alignment.center,
                    color: Colors.teal[100 * (index % 9)],
                    child: Text('Music#$index'),
                  );
                },
                childCount: 99,
              ),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 15,
                crossAxisSpacing: 15,
                childAspectRatio: 2.0,
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.only(bottom: 16.0),
            ),
            SliverAppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.red,
              expandedHeight:200,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Text(
                  'Movies',
                ),
                background: Image.asset(
                  "assets/images/movies.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.only(bottom: 16.0),
            ),
            SliverFixedExtentList(
              itemExtent: 50,
              delegate: SliverChildBuilderDelegate(
                childCount: 100,
                (context, index) => Container(
                  alignment: Alignment.center,
                  height: 100,
                  child: Text('Movie#$index'),
                  color: Color((Random().nextDouble() * 0xFFFFFF).toInt())
                      .withOpacity(1.0),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
