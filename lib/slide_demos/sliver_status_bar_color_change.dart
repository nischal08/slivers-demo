import 'dart:developer';

import 'package:flutter/material.dart';

class SliverStatusbarColorChange extends StatefulWidget {
  const SliverStatusbarColorChange() : super();

  @override
  _SliverStatusbarColorChangeState createState() =>
      _SliverStatusbarColorChangeState();
}

class _SliverStatusbarColorChangeState
    extends State<SliverStatusbarColorChange> {
  late ScrollController _scrollController;
  Color _textColor = Colors.blue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _scrollController = ScrollController()
      ..addListener(() {
        log("hasClients:" + _scrollController.hasClients.toString());
        log("offset:" + _scrollController.offset.toString());
        setState(() {
          _textColor = _isSliverAppBarExpanded ? Colors.white : Colors.blue;
        });
      });
  }

  bool get _isSliverAppBarExpanded {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            snap: true,
            floating: true,
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              titlePadding: const EdgeInsets.only(left: 60, bottom: 10),
              // collapseMode: CollapseMode.pin,
              // expandedTitleScale: 1,
              title: Text(
                'Goa Beach',
                style: TextStyle(
                    color: _textColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ),
              background: Image.asset(
                'assets/images/beach.png',
                fit: BoxFit.fill,
              ),
              stretchModes: [StretchMode.zoomBackground],
            ),
            // collapsedHeight: 100,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (_, int index) {
                return ListTile(
                  leading: Container(
                      padding: EdgeInsets.all(8),
                      width: 100,
                      child: Placeholder()),
                  title: Text('Place ${index + 1}', textScaleFactor: 2),
                );
              },
              childCount: 20,
            ),
          ),
        ],
      ),
    );
  }
}
