import 'package:flutter/material.dart';
import 'package:sliverappbar/movie_detail/movie_profile_page.dart';
import 'package:sliverappbar/slide_demos/all_slivers_basic_page.dart';
import 'package:sliverappbar/slide_demos/basic_sliverappbar.dart';
import 'package:sliverappbar/slide_demos/sliver_status_bar_color_change.dart';
import 'package:sliverappbar/slide_demos/sliverappbar_with_appbar.dart';
import 'package:sliverappbar/slide_demos/sliverappbar_with_tabbar.dart';
import 'package:sliverappbar/slide_demos/stack_sliver_appbar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SliverAppBar Demo',
      theme: ThemeData(
        primarySwatch: Colors.teal,
        brightness: Brightness.dark,
      ),
      home: MyListView(),
    );
  }
}

class MyListView extends StatefulWidget {
  const MyListView({Key? key}) : super(key: key);

  @override
  _MyListViewState createState() => _MyListViewState();
}

class _MyListViewState extends State<MyListView> {
  static const List<Map> _pages = <Map>[
    {
      "screen": BasicSliverAppBar(),
      "name": "Basic Sliver AppBar",
      "level": "Beginner",
    },
    {
      "screen": AllSliversBasicPage(),
      "name": "App Sliver Widgets in one UI",
      "level": "Beginner",
    },
    {
      "screen": SliverStatusbarColorChange(),
      "name": "Appbar color change",
      "level": "Intermediate",
    },
    {
      "screen": SliverAppBarWithAppBar(),
      "name": "Sliver AppBar with AppBar",
      "level": "Beginner",
    },
    {
      "screen": SliverAppBarWithTabBar(),
      "name": "Sliver AppBar with TabBar",
      "level": "Intermediate",
    },
    {
      "screen": StackedAppBarPage(),
      "name": "Sliver AppBar Stacking Effect",
      "level": "Intermediate & Fun",
    },
    {
      "screen": MovieProfilePage(),
      "name": "Movie Detail UI",
      "level": "Advanced",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Sliver AppBar Demo'),
        ),
        body: ListView.builder(
          itemCount: _pages.length,
          itemBuilder: (context, index) => EachItem(
            title: _pages[index]["name"],
            level: _pages[index]["level"],
            onClick: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => _pages[index]["screen"],
                ),
              );
            },
          ),
        ));
  }
}

class EachItem extends StatelessWidget {
  final String title;
  final String level;
  final VoidCallback onClick;
  const EachItem({
    super.key,
    required this.title,
    required this.level,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
        onTap: onClick,
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: new BoxDecoration(
              border: new Border(
                  right: new BorderSide(width: 1.0, color: Colors.white24))),
          child: Icon(Icons.autorenew, color: Colors.white),
        ),
        title: Text(
          title,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: Row(
          children: <Widget>[
            Icon(Icons.linear_scale, color: Colors.yellowAccent),
            SizedBox(
              width: 6,
            ),
            Text(level, style: TextStyle(color: Colors.white))
          ],
        ),
        trailing:
            Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0));
  }
}
