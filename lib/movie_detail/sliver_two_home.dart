import 'package:flutter/material.dart';
import 'package:sliverappbar/movie_detail/movie_profile_page.dart';

import 'models/fake_data.dart';

class SliverTwoHome extends StatelessWidget {
  const SliverTwoHome({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Custom SliverAppBar Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MovieProfilePage(),
    );
  }
}
